package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.TblClientEntity;
import io.renren.modules.generator.entity.TblCompanyEntity;

import java.util.List;
import java.util.Map;

/**
 * 公司表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-12-10 11:16:43
 */
public interface TblCompanyService extends IService<TblCompanyEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<TblCompanyEntity> queryAll(Map<String, Object> params);

}

