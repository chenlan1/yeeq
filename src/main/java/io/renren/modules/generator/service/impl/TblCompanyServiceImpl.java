package io.renren.modules.generator.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.generator.dao.TblCompanyDao;
import io.renren.modules.generator.entity.TblCompanyEntity;
import io.renren.modules.generator.service.TblCompanyService;


@Service("tblCompanyService")
public class TblCompanyServiceImpl extends ServiceImpl<TblCompanyDao, TblCompanyEntity> implements TblCompanyService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TblCompanyEntity> page = this.page(
                new Query<TblCompanyEntity>().getPage(params),
                new QueryWrapper<TblCompanyEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<TblCompanyEntity>queryAll(Map<String, Object> params) {
        return this.listByMap(params);
    }


}