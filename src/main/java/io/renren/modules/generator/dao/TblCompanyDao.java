package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.TblCompanyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 公司表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-12-10 11:16:43
 */
@Mapper
public interface TblCompanyDao extends BaseMapper<TblCompanyEntity> {
	
}
