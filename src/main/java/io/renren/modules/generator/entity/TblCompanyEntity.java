package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 公司表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-12-10 11:16:43
 */
@Data
@TableName("tbl_company")
public class TblCompanyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 公司名称
	 */
	private String name;
	/**
	 * 公司地址
	 */
	private String address;
	/**
	 * 联系人
	 */
	private String linkName;

	private Integer sex;
	/**
	 * 联系电话
	 */
	private String linkTelephone;
	/**
	 * 身份证号
	 */
	private String linkCardNo;
	/**
	 * 创建时间
	 */
	private Date createrTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 操作人
	 */
	private String operatorName;

	private String comment;
	/**
	 * 
	 */
	private Integer state;

}
