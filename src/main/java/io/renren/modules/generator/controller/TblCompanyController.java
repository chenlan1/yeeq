package io.renren.modules.generator.controller;

import java.util.*;

import io.renren.modules.generator.entity.TblClientEntity;
import io.renren.modules.generator.vo.resp.CompanyKeyValueVo;
import io.renren.modules.generator.vo.resp.SelectClientVo;
import io.renren.modules.sys.controller.AbstractController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.generator.entity.TblCompanyEntity;
import io.renren.modules.generator.service.TblCompanyService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 公司表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-12-10 11:16:43
 */
@RestController
@RequestMapping("generator/tblcompany")
public class TblCompanyController extends AbstractController {
    @Autowired
    private TblCompanyService tblCompanyService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:tblcompany:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = tblCompanyService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/allList")
    @RequiresPermissions("generator:tblcompany:list")
    public R allList(@RequestParam Map<String, Object> params){
        List<TblCompanyEntity> tblCompanyEntityList= tblCompanyService.queryAll(params);
        List<CompanyKeyValueVo> companyList = new ArrayList<>();
        for (TblCompanyEntity companyEntity:tblCompanyEntityList) {
            CompanyKeyValueVo companyVo = new CompanyKeyValueVo();
            BeanUtils.copyProperties(companyEntity, companyVo);
            companyVo.setTelephone(companyEntity.getLinkTelephone());
            companyList.add(companyVo);
        }
        return R.ok().put("clientList", companyList);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:tblcompany:info")
    public R info(@PathVariable("id") Integer id){
		TblCompanyEntity tblCompany = tblCompanyService.getById(id);

        return R.ok().put("tblCompany", tblCompany);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:tblcompany:save")
    public R save(@RequestBody TblCompanyEntity tblCompany){
        Date curDate = new Date();
        tblCompany.setCreaterTime(curDate);
        tblCompany.setUpdateTime(curDate);
        tblCompany.setOperatorName(getUserName());
		tblCompanyService.save(tblCompany);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("generator:tblcompany:update")
    public R update(@RequestBody TblCompanyEntity tblCompany){
        tblCompany.setUpdateTime(new Date());
        tblCompany.setOperatorName(getUserName());
		tblCompanyService.updateById(tblCompany);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:tblcompany:delete")
    public R delete(@RequestBody Integer[] ids){
		tblCompanyService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
