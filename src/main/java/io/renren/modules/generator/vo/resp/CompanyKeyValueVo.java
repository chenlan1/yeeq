package io.renren.modules.generator.vo.resp;

import lombok.Data;

import java.io.Serializable;

@Data
public class CompanyKeyValueVo implements Serializable {

    private Integer id;
    /**
     * 公司名称
     */
    private String name;

    /**
     * 联系人
     */
    private String linkName;

    /**
     * 联系电话
     */
    private String telephone;
}
