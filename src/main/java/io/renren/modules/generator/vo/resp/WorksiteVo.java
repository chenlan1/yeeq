package io.renren.modules.generator.vo.resp;

import io.renren.modules.generator.entity.TblWorksiteEntity;
import lombok.Data;

@Data
public class WorksiteVo extends TblWorksiteEntity {
    /**
     * 客户/公司名称
     */
    private String clientName;

    /**
     * 联系电话
     */
    private String worksiteTelephone;

    /**
     * 授权人
     */
    private String link_name;


}
